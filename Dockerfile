FROM centos:centos7
RUN yum -y update; 
RUN yum install -y gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel wget make
RUN cd /usr/src; wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz ; tar xzf Python-3.7.11.tgz; cd Python-3.7.11; \
    ./configure --enable-optimizations && make altinstall 
RUN /usr/local/bin/python3.7 -m pip install --upgrade pip
RUN /usr/local/bin/python3.7 -m pip install flask flask-jsonpify flask-restful

COPY python-api.py /opt/api/python-api.py

CMD ["python3.7","/opt/api/python-api.py"]
#CMD /bin/sh

